import {Suspense} from "react";
import Loading from "./Loading";

const SuspensefulLoader = ({children}) => {
  return (
    <Suspense fallback={<Loading/>}>
      {children}
    </Suspense>
  )
}

export default SuspensefulLoader