import {createContext} from "react";
import useDummyTableService from "./useDummyTableService";

export const ServiceContext = createContext({})
const ServiceProvider = ({children}) => {
  const services = {
    tableService: useDummyTableService(),
    tableService2: useDummyTableService(),
  }
  return (
    <ServiceContext.Provider value={services}>
      {children}
    </ServiceContext.Provider>
  )
}
export default ServiceProvider;