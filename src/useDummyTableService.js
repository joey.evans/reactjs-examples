import {wrapPromise} from "./utils";

const useDummyTableService = () => {
  const promise = new Promise(resolve => setTimeout(resolve, 2500))
    .then(() => {
      return [
        {name:"first", value:"ONE"},
        {name:"second", value:"TWO"},
        {name:"third", value:"THREE"}
      ]
    })
  return wrapPromise(promise)
}

export default useDummyTableService