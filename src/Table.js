import {useContext} from "react";
import {ServiceContext} from "./ServiceProvider";
import styled from "styled-components";
import SuspensefulLoader from "./SuspensefulLoading";

const StyledTable = styled('table')`
  border:1px solid black;
`

export const SuspensefulTable = () => {
  return (
    <SuspensefulLoader>
      <Table/>
    </SuspensefulLoader>
  )
}

const Table = ()=>{
  const {tableService} = useContext(ServiceContext);
  const values = tableService.read()
  return (
    <StyledTable>
      <thead>
      <tr>
        <th>Name</th>
        <th>Value</th>
      </tr>
      </thead>
      <tbody>
      {values.map((val, i)=> {
        return (
          <tr key={i}>
            <td>{val.name}</td>
            <td>{val.value}</td>
          </tr>
        )
      })}
      </tbody>
    </StyledTable>
  )
}

export default Table